#------------------------------------------------------------------------------------------------------------------------------------
# Copyright 2014 Fabrice Roy
#
# Contact: fabrice.roy@obspm.fr
#
# This file is part of fortran-hdf5-interface.
#
# fortran-hdf5-interface is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fortran-hdf5-interface is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.
#
# Make.inc for GNU compilers
# Author Fabrice Roy
# ======================================================================

FC=h5pfc
MPIFC=h5pfc

# PATH TO INSTALL DIRECTORIES
PREFIX=/opt/fhi/1.0/hdf5/1.12.0/openmpi/4.0.4/gcc/10.2.0

# Optional precompiler options used in the code:
# -DDEBUGHDF5: print some debug from HDF5 and crashes in case of errors
# -DSERIALHDF5: only compile serial part of the library

# GNU release flags
FCFLAGS= -O3 -g

# GNU debug flags
DEBUGFLAGS= -O0 -g -Wall -Wextra -finit-real=zero -finit-integer=0 -std=f2008 -fcheck=all -fbacktrace -fmax-errors=10 -DDEBUGHDF5

LDFLAGS=-g
