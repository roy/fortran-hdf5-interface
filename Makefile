#------------------------------------------------------------------------------------------------------------------------------------
# Copyright 2014 Fabrice Roy
#
# Contact: fabrice.roy@obspm.fr
#
# This file is part of fortran-hdf5-interface.
#
# fortran-hdf5-interface is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fortran-hdf5-interface is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.
#
# Makefile for the library
# Author Fabrice Roy
# ======================================================================


include Make.inc

# Install command
INSTALL=install -c -m 644
export INSTALL

FHILIBDIR = $(PREFIX)/lib
FHIMODDIR = $(PREFIX)/include
export FHILIBDIR
export FHIMODDIR

# Rules
# Remove implicit rules
.SUFFIXES:

all: libs

debug: libs_debug

libs : 
	cd src && make

libs_debug:
	cd src && make debug

# TESTS need debug libraries
test:	libs_debug testserial testmpi
testserial: 
	cd test && make serial
testmpi:
	cd test && make mpi

clean :
	@echo "--------------------------------"
	@echo cleaning compiled objects and tests
	cd src && make clean
	cd test && make clean
	cd doc && make clean

doc:
	cd doc && make

doc_html : 
	cd doc && make html

installdirs: 
	./mkinstalldirs $(FHILIBDIR) $(FHIMODDIR)

install: installdirs
	cd src && make install
	
uninstall:
	rm -rf $(PREFIX)

.PHONY: all debug libs libs_debug test testserial testmpi clean doc inst install install_libs install_mod
