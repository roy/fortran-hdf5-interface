!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2014 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of fortran-hdf5-interface.
!
! fortran-hdf5-interface is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! fortran-hdf5-interface is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.
!
! MODULE: fortran_hdf5_read_data_m
!
!> @author
!> Fabrice Roy (LUTH, CNRS, Paris Observatory, PSL University)
!
!> @brief
!> Subroutines to read data from a HDF5 serial file
!
!> @details
!> Use the Hdf5_read_data interface to read data from a serial file.
!> The data can be a 1-D or 2-D array of integer(4), integer(8), real(4) or real(8), or an integer(8) scalar.
!> The specific subroutines are private.
!
!> @date
!> 11 jan 2019: initial version
!======================================================================

module fortran_hdf5_read_data_m

  use fortran_hdf5_constants_m
  use hdf5
  use iso_fortran_env
  
  implicit none

  private

  public :: Hdf5_read_data

  !> Generic inteface used to read data from a hdf5 file.
  !> @param[in] id id of the file/group where the dataset will be read
  !> @param[in] name name of the dataset
  !> @param[in] n1 first dimension of the data array
  !> @param[in] n2 second dimension of the data array (optional)
  !> @param[in,out] data 1-D or 2-D data array of type integer(4), integer(8), real(4) or real(8)
  interface Hdf5_read_data
     module procedure Hdf5_read_int4_0D        ! (id, name, data)
     module procedure Hdf5_read_int4_1D        ! (id, name, n1, data)
     module procedure Hdf5_read_int4_2D        ! (id, name, n1, n2, data)
     module procedure Hdf5_read_int8_0D        ! (id, name, data)
     module procedure Hdf5_read_int8_1D        ! (id, name, n1, data)
     module procedure Hdf5_read_int8_2D        ! (id, name, n1, n2, data)
     module procedure Hdf5_read_real4_1D       ! (id, name, n1, data)
     module procedure Hdf5_read_real4_2D       ! (id, name, n1, n2, data)
     module procedure Hdf5_read_real8_1D       ! (id, name, n1, data)
     module procedure Hdf5_read_real8_2D       ! (id, name, n1, n2, data)
  end interface Hdf5_read_data

  integer, parameter :: ERR_MSG_LEN=256
  
contains

  !=======================================================================
  !> Read a 0-D integer(kind=4) scalar from a serial file
  subroutine Hdf5_read_int4_0D(id, name, data)

    integer(hid_t), intent(in) :: id                               !< id of the file/group where the dataset will be read
    character(len=H5_STR_LEN), intent(in) :: name                          !< name of the dataset
    integer(kind=4), intent(inout), target :: data  !< array

    integer(hid_t) :: dset_id                               ! id of the dataset
    integer(hid_t) :: h5_kind                               ! HDF5 integer type
    integer :: h5err
    type(c_ptr) :: ptr

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_int4_0D begins for data ', trim(name)
#endif

    ptr = c_loc(data)
    ! hdf5 type corresponding to the integer type to read
    h5_kind = h5kind_to_type(4,H5_INTEGER_KIND)

    ! open the dataset
    call h5dopen_f(id, name, dset_id, h5err)

    ! read the dataset
    call h5dread_f(dset_id, h5_kind, ptr, h5err)

    ! close the dataset
    call h5dclose_f(dset_id, h5err)

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_int4_0D ends for data ', trim(name)
#endif

  end subroutine Hdf5_read_int4_0D

  !=======================================================================
  !> @author
  !> Fabrice Roy
  !
  !> @brief
  !> Read a 1-D array of integer(4) data given its name, its parent id, and its size, returns the data
  !
  !> @param[in] id : hdf5 identifier of the parents of the data
  !> @param[in] name : name of the attribute 
  !> @param[in] n1 : size of the array in the 1st dimension
  !> @param[inout] data : data read
  subroutine Hdf5_read_int4_1D(id, name, n1, data)

    integer(hid_t), intent(in) :: id
    character(len=H5_STR_LEN), intent(in) :: name
    integer(kind=4), intent(in) :: n1
    integer(kind=4), dimension(n1), intent(inout), target :: data

    integer(hid_t) :: dset_id                               ! id of the dataset
    integer(hid_t) :: h5_kind                               ! HDF5 integer type
    integer :: h5err
    type(c_ptr) :: ptr

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_int4_1D begins for data ', trim(name)
#endif

    ptr = c_loc(data(1))
    ! hdf5 type corresponding to the integer type to read
    h5_kind = h5kind_to_type(4,H5_INTEGER_KIND)

    ! open the dataset
    call h5dopen_f(id, name, dset_id, h5err)
    if(h5err < 0) then
       write(ERROR_UNIT,*) 'h5dopen_f failed for ',name
    end if

    ! read the dataset
    call h5dread_f(dset_id, h5_kind, ptr, h5err)
    if(h5err < 0) then
       write(ERROR_UNIT,*) 'h5dread_f failed for ',name
       write(ERROR_UNIT,*) 'size of the data array ',size(data)
    end if

    ! close the dataset
    call h5dclose_f(dset_id, h5err)

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_int4_1D ends for data ',name
#endif

  end subroutine Hdf5_read_int4_1D

  !=======================================================================
  !> Read a 1-D integer(kind=8) scalar from a serial file
  subroutine Hdf5_read_int8_0D(id, name, data)

    integer(hid_t), intent(in) :: id                               !< id of the file/group where the dataset will be read
    character(len=H5_STR_LEN), intent(in) :: name                          !< name of the dataset
    integer(kind=8), intent(inout), target :: data  !< array

    integer(hid_t) :: dset_id                               ! id of the dataset
    integer(hid_t) :: h5_kind                               ! HDF5 integer type
    integer :: h5err
    type(c_ptr) :: ptr

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_int8_0D begins for data ', trim(name)
#endif

    ptr = c_loc(data)
    ! hdf5 type corresponding to the integer type to read
    h5_kind = h5kind_to_type(8,H5_INTEGER_KIND)

    ! open the dataset
    call h5dopen_f(id, name, dset_id, h5err)

    ! read the dataset
    call h5dread_f(dset_id, h5_kind, ptr, h5err)

    ! close the dataset
    call h5dclose_f(dset_id, h5err)

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_int8_0D ends for data ', trim(name)
#endif

  end subroutine Hdf5_read_int8_0D


  !=======================================================================
  !> Read a 1-D integer(kind=8) array from a serial file
  subroutine Hdf5_read_int8_1D(id, name, n1, data)

    integer(hid_t), intent(in) :: id                               !< id of the file/group where the dataset will be read
    character(len=H5_STR_LEN), intent(in) :: name                          !< name of the dataset
    integer(kind=4), intent(in) :: n1                              !< dimension of the array to read
    integer(kind=8), dimension(n1), intent(inout), target :: data  !< array

    integer(hid_t) :: dset_id                               ! id of the dataset
    integer(hid_t) :: h5_kind                               ! HDF5 integer type
    integer :: h5err
    type(c_ptr) :: ptr

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_int8_1D begins for data ', trim(name)
#endif

    ptr = c_loc(data(1))
    ! hdf5 type corresponding to the integer type to read
    h5_kind = h5kind_to_type(8,H5_INTEGER_KIND)

    ! open the dataset
    call h5dopen_f(id, name, dset_id, h5err)

    ! read the dataset
    call h5dread_f(dset_id, h5_kind, ptr, h5err)

    ! close the dataset
    call h5dclose_f(dset_id, h5err)

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_int8_1D ends for data ', trim(name)
#endif

  end subroutine Hdf5_read_int8_1D


  !=======================================================================
  !> Read a 2-D integer(kind=4) array from a serial file
  subroutine Hdf5_read_int4_2D(id, name, n1, n2, data)

    integer(hid_t), intent(in) :: id                                   !< id of the file/group where the dataset will be read
    character(len=H5_STR_LEN), intent(in) :: name                              !< name of the dataset
    integer(kind=4), intent(in) :: n1                                  !< first dimension of the array to read
    integer(kind=4), intent(in) :: n2                                  !< second dimension of the array to read
    integer(kind=4), dimension(n1,n2), intent(inout), target :: data   !< array

    integer(hid_t) :: dset_id                               ! id of the dataset
    integer(hid_t) :: h5_kind                               ! HDF5 integer type
    integer :: h5err
    type(c_ptr) :: ptr

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_int4_2D begins for data ', trim(name)
#endif

    ptr = c_loc(data(1,1))
    ! hdf5 type corresponding to the integer type to read
    h5_kind = h5kind_to_type(4,H5_INTEGER_KIND)

    ! open the dataset
    call h5dopen_f(id, name, dset_id, h5err)

    ! read the dataset
    call h5dread_f(dset_id, h5_kind, ptr, h5err)

    ! close the dataset
    call h5dclose_f(dset_id, h5err)

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_int4_2D ends for data ', trim(name)
#endif

  end subroutine Hdf5_read_int4_2D


  !=======================================================================
  !> Read a 2-D integer(kind=8) array from a serial file
  subroutine Hdf5_read_int8_2D(id, name, n1, n2, data)

    integer(hid_t), intent(in) :: id                                  !< id of the file/group where the dataset will be read
    character(len=H5_STR_LEN), intent(in) :: name                             !< name of the dataset
    integer(kind=4), intent(in) :: n1                                 !< first dimension of the array to read
    integer(kind=4), intent(in) :: n2                                 !< second dimension of the array to read
    integer(kind=8), dimension(n1,n2), intent(inout), target :: data  !< array

    integer(hid_t) :: dset_id                               ! id of the dataset
    integer(hid_t) :: h5_kind                               ! HDF5 integer type
    integer :: h5err
    type(c_ptr) :: ptr

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_int8_2D begins for data ', trim(name)
#endif

    ptr = c_loc(data(1,1))
    ! hdf5 type corresponding to the integer type to read
    h5_kind = h5kind_to_type(8,H5_INTEGER_KIND)

    ! open the dataset
    call h5dopen_f(id, name, dset_id, h5err)

    ! read the dataset
    call h5dread_f(dset_id, h5_kind, ptr, h5err)

    ! close the dataset
    call h5dclose_f(dset_id, h5err)

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_int8_2D ends for data ', trim(name)
#endif

  end subroutine Hdf5_read_int8_2D


  !=======================================================================
  !> Read a 1-D real(kind=4) array from a serial file
  subroutine Hdf5_read_real4_1D(id, name, n1, data)

    integer(hid_t), intent(in) :: id                            !< id of the file/group where the dataset will be read
    character(len=H5_STR_LEN), intent(in) :: name                       !< name of the dataset
    integer(kind=4), intent(in) :: n1                           !< dimension of the array to read
    real(kind=4), dimension(n1), intent(inout), target :: data  !< array

    integer(hid_t) :: dset_id                               ! id of the dataset
    integer(hid_t) :: h5_kind                               ! HDF5 integer type
    integer :: h5err
    type(c_ptr) :: ptr

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_real4_1D begins for data ', trim(name)
#endif

    ptr = c_loc(data(1))
    ! hdf5 type corresponding to the integer type to read
    h5_kind = h5kind_to_type(4,H5_REAL_KIND)

    ! open the dataset
    call h5dopen_f(id, name, dset_id, h5err)

    ! read the dataset
    call h5dread_f(dset_id, h5_kind, ptr, h5err)

    ! close the dataset
    call h5dclose_f(dset_id, h5err)

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_real4_1D ends for data ', trim(name)
#endif

  end subroutine Hdf5_read_real4_1D


  !=======================================================================
  !> Read a 1-D real(kind=8) array from a serial file
  subroutine Hdf5_read_real8_1D(id, name, n1, data)

    integer(hid_t), intent(in) :: id                            !< id of the file/group where the dataset will be read
    character(len=H5_STR_LEN), intent(in) :: name                       !< name of the dataset
    integer(kind=4), intent(in) :: n1                           !< dimension of the array to read
    real(kind=8), dimension(n1), intent(inout), target :: data  !< array

    integer(hid_t) :: dset_id                               ! id of the dataset
    integer(hid_t) :: h5_kind                               ! HDF5 integer type
    integer :: h5err
    type(c_ptr) :: ptr

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_real8_1D begins for data ', trim(name)
#endif

    ptr = c_loc(data(1))
    ! hdf5 type corresponding to the integer type to read
    h5_kind = h5kind_to_type(8,H5_REAL_KIND)

    ! open the dataset
    call h5dopen_f(id, name, dset_id, h5err)

    ! read the dataset
    call h5dread_f(dset_id, h5_kind, ptr, h5err)

    ! close the dataset
    call h5dclose_f(dset_id, h5err)

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_real8_1D ends for data ', trim(name)
#endif

  end subroutine Hdf5_read_real8_1D


  !=======================================================================
  !> Read a 2-D real(kind=4) array from a serial file
  subroutine Hdf5_read_real4_2D(id, name, n1, n2, data)

    integer(hid_t), intent(in) :: id                               !< id of the file/group where the dataset will be read
    character(len=H5_STR_LEN), intent(in) :: name                          !< name of the dataset
    integer(kind=4), intent(in) :: n1                              !< first dimension of the array to read
    integer(kind=4), intent(in) :: n2                              !< second dimension of the array to read
    real(kind=4), dimension(n1,n2), intent(inout), target :: data  !< array

    integer(hid_t) :: dset_id                               ! id of the dataset
    integer(hid_t) :: h5_kind                               ! HDF5 integer type
    integer :: h5err
    type(c_ptr) :: ptr

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_real4_2D begins for data ', trim(name)
#endif

    ptr = c_loc(data(1,1))
    ! hdf5 type corresponding to the integer type to read
    h5_kind = h5kind_to_type(4,H5_REAL_KIND)

    ! open the dataset
    call h5dopen_f(id, name, dset_id, h5err)

    ! read the dataset
    call h5dread_f(dset_id, h5_kind, ptr, h5err)

    ! close the dataset
    call h5dclose_f(dset_id, h5err)

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_real4_2D ends for data ', trim(name)
#endif

  end subroutine Hdf5_read_real4_2D


  !=======================================================================
  !> Read a 2-D real(kind=8) array from a serial file
  subroutine Hdf5_read_real8_2D(id, name, n1, n2, data)

    integer(hid_t), intent(in) :: id                               !< id of the file/group where the dataset will be read
    character(len=H5_STR_LEN), intent(in) :: name                          !< name of the dataset
    integer(kind=4), intent(in) :: n1                              !< first dimension of the array to read
    integer(kind=4), intent(in) :: n2                              !< second dimension of the array to read
    real(kind=8), dimension(n1,n2), intent(inout), target :: data  !< array

    integer(hid_t) :: dset_id                               ! id of the dataset
    integer(hid_t) :: h5_kind                               ! HDF5 integer type
    integer :: h5err
    type(c_ptr) :: ptr

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_real8_2D begins for data ', trim(name)
#endif

    ptr = c_loc(data(1,1))
    ! hdf5 type corresponding to the integer type to read
    h5_kind = h5kind_to_type(8,H5_REAL_KIND)

    ! open the dataset
    call h5dopen_f(id, name, dset_id, h5err)

    ! read the dataset
    call h5dread_f(dset_id, h5_kind, ptr, h5err)

    ! close the dataset
    call h5dclose_f(dset_id, h5err)

#ifdef DEBUGHDF5
    write(ERROR_UNIT,*) 'Hdf5_read_real8_2D ends for data ', trim(name)
#endif

  end subroutine Hdf5_read_real8_2D
  
  
end module fortran_hdf5_read_data_m
