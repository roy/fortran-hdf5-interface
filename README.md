# Fortran HDF5 interface

Simplified Fortran interface for HDF5 I/O.  
This library provides wrappers that allow to write and read HDF5 files with Fortran.  

## Build

### Requirements

Serial version:
- HDF5 1.8.x built with --enable-fortran2003
- HDF5 1.10.x built with --enable-fortran 

Parallel version:
- a MPI distribution
- HDF5 1.8.x built with --enable-parallel --enable-fortran2003
- HDF5 1.10.x build with --enable-parallel --enable-fortran

### Makefile

The library comes with a standard GNU Makefile.  
- `make` builds the parallel (default) version.  
- `make serial` builds the serial version.

You can also build debug versions with `make debug`.

## Documentation
Documentation can be found [here](https://roy.pages.obspm.fr/fortran-hdf5-interface/index.html).

### Quick start guide

Example:
```fortran
program mycode

use fortran_hdf5_constants
use fortran_hdf5_manage_interface
use fortran_hdf5_manage_files
use fortran_hdf5_write_data
use hdf5

implicit none

integer(hid_t) :: file_id
integer(kind=4), dimension(3) :: data_integer4
character(H5_FILENAME_LEN) :: file_name

call Hdf5_init()

filename = 'myresults.h5'
call Hdf5_create_file(filename,file_id)

data_name = 'mydata_integer'
data_integer4 = (/1, 2, 3/)
call Hdf5_write_data(file_id, data_name, 3, dim(data_integer4), data_integer4)

call Hdf5_close_file(file_id)
call Hdf5_finalize()

end program mycode
```
