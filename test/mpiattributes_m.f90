!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2014 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of fortran-hdf5-interface.
!
! fortran-hdf5-interface is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! fortran-hdf5-interface is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Tests for MPI version of fortran-hdf5-interface: read and write attributes
!! @brief
!!
!! @author Fabrice Roy
! ======================================================================

module mpiattributes_m

  implicit none

  private

  public :: Read_mpi_attributes, &
       Write_mpi_attributes

  integer(kind=4),                 parameter :: attr_int4_dim0_w  = 0
  integer(kind=4), dimension(3),   parameter :: attr_int4_dim1_w  = 1
  integer(kind=4), dimension(4,5), parameter :: attr_int4_dim2_w  = 2
  real(kind=4),                    parameter :: attr_real4_dim0_w = 3_4
  real(kind=4), dimension(3),      parameter :: attr_real4_dim1_w = 4_4
  real(kind=4), dimension(4,5),    parameter :: attr_real4_dim2_w = 5_4
  real(kind=8),                    parameter :: attr_real8_dim0_w = 6_8
  real(kind=8), dimension(3),      parameter :: attr_real8_dim1_w = 7_8
  real(kind=8), dimension(4,5),    parameter :: attr_real8_dim2_w = 8_8

  integer :: nb_attr, success_attr

contains

!------------------------------------------------------------------------------------------------------------------------------------
  subroutine Read_mpi_attributes(id)

    use fortran_hdf5_constants_m
    use fortran_hdf5_read_attribute_m
    use hdf5
    use iso_fortran_env

    integer(hid_t), intent(in) :: id
    
    character(H5_STR_LEN) :: attr_name
    integer(kind=4) :: attr_int4_dim0_r
    integer(kind=4), dimension(3) :: attr_int4_dim1_r
    integer(kind=4), dimension(4,5) :: attr_int4_dim2_r
    integer :: idim, jdim
    logical :: is_success
    real(kind=4) :: attr_real4_dim0_r
    real(kind=4), dimension(3) :: attr_real4_dim1_r
    real(kind=4), dimension(4,5) :: attr_real4_dim2_r
    real(kind=8) :: attr_real8_dim0_r
    real(kind=8), dimension(3) :: attr_real8_dim1_r
    real(kind=8), dimension(4,5) :: attr_real8_dim2_r

    
    success_attr=0

    attr_name = 'integer4_dim0'
    call Hdf5_read_attr(id, attr_name, attr_int4_dim0_r)
    if( attr_int4_dim0_r /= attr_int4_dim0_w ) then
       write(ERROR_UNIT,*) 'Error: attr_int4_dim0_r /= attr_int4_dim0_w'
    else
       success_attr=success_attr+1
    end if

    attr_name = 'integer4_dim1'
    call Hdf5_read_attr(id, attr_name, size(attr_int4_dim1_r), attr_int4_dim1_r)
    is_success=.true.
    do idim=1, size(attr_int4_dim1_w)
       if( attr_int4_dim1_r(idim) /= attr_int4_dim1_w(idim) ) then
          write(ERROR_UNIT,*) 'Error: attr_int4_dim1_r /= attr_int4_dim1_w'
          is_success=.false.
          cycle
       end if
    end do
    if(is_success) success_attr=success_attr+1

    attr_name = 'integer4_dim2'
    call Hdf5_read_attr(id, attr_name, size(attr_int4_dim2_r,1), size(attr_int4_dim2_r,2), attr_int4_dim2_r)
    is_success=.true.
    do jdim=1, size(attr_int4_dim2_w,2)
       do idim=1, size(attr_int4_dim2_w,1)        
          if( attr_int4_dim2_r(idim,jdim) /= attr_int4_dim2_w(idim,jdim) ) then
             write(ERROR_UNIT,*) 'Error: attr_int4_dim2_r /= attr_int4_dim2_w'
             is_success=.false.
             cycle
          end if
       end do
    end do
    if(is_success) success_attr=success_attr+1

    attr_name = 'real4_dim0'
    call Hdf5_read_attr(id, attr_name, attr_real4_dim0_r)
    if( attr_real4_dim0_r /= attr_real4_dim0_w ) then
       write(ERROR_UNIT,*) 'Error: attr_real4_dim0_r /= attr_real4_dim0_w'
    else
       success_attr=success_attr+1
    end if

    attr_name = 'real4_dim1'
    call Hdf5_read_attr(id, attr_name, size(attr_real4_dim1_r), attr_real4_dim1_r)
    is_success=.true.
    do idim=1, size(attr_real4_dim1_w)
       if( attr_real4_dim1_r(idim) /= attr_real4_dim1_w(idim) ) then
          write(ERROR_UNIT,*) 'Error: attr_real4_dim1_r /= attr_real4_dim1_w'
          is_success=.false.
          cycle
       end if
    end do
    if(is_success) success_attr=success_attr+1

    attr_name = 'real4_dim2'
    call Hdf5_read_attr(id, attr_name, size(attr_real4_dim2_r,1), size(attr_real4_dim2_r,2), attr_real4_dim2_r)
    is_success=.true.
    do jdim=1, size(attr_real4_dim2_w,2)
       do idim=1, size(attr_real4_dim2_w,1)        
          if( attr_real4_dim2_r(idim,jdim) /= attr_real4_dim2_w(idim,jdim) ) then
             write(ERROR_UNIT,*) 'Error: attr_real4_dim2_r /= attr_real4_dim2_w'
             is_success=.false.
             cycle
          end if
       end do
    end do
    if(is_success) success_attr=success_attr+1

    attr_name = 'real8_dim0'
    call Hdf5_read_attr(id, attr_name, attr_real8_dim0_r)
    if( attr_real8_dim0_r /= attr_real8_dim0_w ) then
       write(ERROR_UNIT,*) 'Error: attr_real8_dim0_r /= attr_real8_dim0_w'
    else
       success_attr=success_attr+1
    end if

    attr_name = 'real8_dim1'
    call Hdf5_read_attr(id, attr_name, size(attr_real8_dim1_r), attr_real8_dim1_r)
    is_success=.true.
    do idim=1, size(attr_real8_dim1_w)
       if( attr_real8_dim1_r(idim) /= attr_real8_dim1_w(idim) ) then
          write(ERROR_UNIT,*) 'Error: attr_real8_dim1_r /= attr_real8_dim1_w'
          is_success=.false.
          cycle
       end if
    end do
    if(is_success) success_attr=success_attr+1

    attr_name = 'real8_dim2'
    call Hdf5_read_attr(id, attr_name, size(attr_real8_dim2_r,1), size(attr_real8_dim2_r,2), attr_real8_dim2_r)
    is_success=.true.
    do jdim=1, size(attr_real8_dim2_w,2)
       do idim=1, size(attr_real8_dim2_w,1)        
          if( attr_real8_dim2_r(idim,jdim) /= attr_real8_dim2_w(idim,jdim) ) then
             write(ERROR_UNIT,*) 'Error: attr_real8_dim2_r /= attr_real8_dim2_w'
             is_success=.false.
             cycle
          end if
       end do
    end do
    if(is_success) success_attr=success_attr+1

    write(OUTPUT_UNIT,'(a,i2,a,i2)') 'Tests attributes: ',success_attr,'/',nb_attr

  end subroutine Read_mpi_attributes


!------------------------------------------------------------------------------------------------------------------------------------
  subroutine Write_mpi_attributes(id)

    use fortran_hdf5_constants_m
    use fortran_hdf5_write_attribute_m
    use hdf5
    use iso_fortran_env

    integer(hid_t), intent(in) :: id

    character(H5_STR_LEN) :: attr_name

    nb_attr=0

    attr_name = 'integer4_dim0' 
    call Hdf5_write_attr(id, attr_name, attr_int4_dim0_w)
    nb_attr = nb_attr+1

    attr_name = 'integer4_dim1' 
    call Hdf5_write_attr(id, attr_name, size(attr_int4_dim1_w), attr_int4_dim1_w)
    nb_attr = nb_attr+1

    attr_name = 'integer4_dim2'
    call Hdf5_write_attr(id, attr_name, size(attr_int4_dim2_w,1),size(attr_int4_dim2_w,2), attr_int4_dim2_w)
    nb_attr = nb_attr+1

    attr_name = 'real4_dim0' 
    call Hdf5_write_attr(id, attr_name, attr_real4_dim0_w)
    nb_attr = nb_attr+1

    attr_name = 'real4_dim1' 
    call Hdf5_write_attr(id, attr_name, size(attr_real4_dim1_w), attr_real4_dim1_w)
    nb_attr = nb_attr+1

    attr_name = 'real4_dim2'
    call Hdf5_write_attr(id, attr_name, size(attr_real4_dim2_w,1),size(attr_real4_dim2_w,2), attr_real4_dim2_w)
    nb_attr = nb_attr+1

    attr_name = 'real8_dim0' 
    call Hdf5_write_attr(id, attr_name, attr_real8_dim0_w)
    nb_attr = nb_attr+1

    attr_name = 'real8_dim1' 
    call Hdf5_write_attr(id, attr_name, size(attr_real8_dim1_w), attr_real8_dim1_w)
    nb_attr = nb_attr+1

    attr_name = 'real8_dim2'
    call Hdf5_write_attr(id, attr_name, size(attr_real8_dim2_w,1),size(attr_real8_dim2_w,2), attr_real8_dim2_w)
    nb_attr = nb_attr+1

  end subroutine Write_mpi_attributes


end module mpiattributes_m
