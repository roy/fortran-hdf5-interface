!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2014 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of fortran-hdf5-interface.
!
! fortran-hdf5-interface is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! fortran-hdf5-interface is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Tests for serial version of fortran-hdf5-interface
!! @brief
!!
!! @author Fabrice Roy
! ======================================================================

program test_serial

  use fortran_hdf5_constants_m
  use fortran_hdf5_manage_files_m
  use fortran_hdf5_manage_groups_m
  use fortran_hdf5_manage_interface_m
  use hdf5
  use iso_fortran_env
  use read_write_attributes_m
  use read_serial_data_m
  use write_serial_data_m

  implicit none

  integer(hid_t) :: file_id
  character(H5_FILENAME_LEN) :: file_name
  integer(hid_t) :: group_id, group2_id
  character(H5_STR_LEN) :: group_name
  logical :: link_exists
  character(H5_STR_LEN),allocatable :: groups(:)
  integer,allocatable :: types(:)


  file_name='test_serial.h5'

  call Hdf5_init()

  ! Create file and write into it
  call Hdf5_create_file(file_name, file_id)

  group_name = 'attributes'
  call Hdf5_create_group(file_id, group_name, group_id)

  call Write_attributes(group_id)

  call Hdf5_close_group(group_id)

  group_name = 'data'
  call Hdf5_create_group(file_id, group_name, group_id)

  group_name = 'integer'
  call Hdf5_create_group(group_id, group_name, group2_id)

  call Write_serial_int(group2_id)

  call Hdf5_close_group(group2_id)

  group_name = 'real'
  call Hdf5_create_group(group_id, group_name, group2_id)

  call Write_serial_real(group2_id)

  call Hdf5_close_group(group2_id)

  call Hdf5_close_group(group_id)

  call Hdf5_close_file(file_id)

  ! Open file, read it and check the values
  call Hdf5_open_file(file_name, file_id)

  group_name = 'attributes'
  call Hdf5_open_group(file_id, group_name, group_id)

  call Read_attributes(group_id)

  call Hdf5_close_group(group_id)

  group_name = 'real'
  call Hdf5_query_group(file_id,group_name,link_exists)
  if(link_exists)then
     call Hdf5_find_groups(file_id,group_name,groups,types)
     print *, groups, types

  endif

  call Hdf5_close_file(file_id)

  call Hdf5_finalize()


end program test_serial
