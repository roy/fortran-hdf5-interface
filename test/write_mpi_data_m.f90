!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2014 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of fortran-hdf5-interface.
!
! fortran-hdf5-interface is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! fortran-hdf5-interface is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Tests for MPI version of fortran-hdf5-interface: write data
!! @brief
!!
!! @author Fabrice Roy
! ======================================================================

module write_mpi_data_m

  implicit none

  private

  public :: Write_mpi_int, &
       Write_mpi_real
  
contains

  subroutine Write_mpi_int(id)

    use hdf5
    
    integer(hid_t), intent(in) :: id


  end subroutine Write_mpi_int

!------------------------------------------------------------------------------------------------------------------------------------

  subroutine Write_mpi_real(id)

    use fortran_hdf5_constants_m
    use fortran_hdf5_write_mpi_data_m
    use hdf5
    use mpi
    use data_m
    
    integer(hid_t), intent(in) :: id

    character(H5_STR_LEN) :: name

    call init_data
    
    name = 'real4_1d'
    call Hdf5_write_mpi_data(id, name, N1, data_real4_1d, MPI_COMM_WORLD)

    name = 'real4_2d'
    call Hdf5_write_mpi_data(id, name, N1, N2, data_real4_2d, MPI_COMM_WORLD)

    name = 'real8_1d'
    call Hdf5_write_mpi_data(id, name, N1, data_real8_1d, MPI_COMM_WORLD)

    name = 'real8_2d'
    call Hdf5_write_mpi_data(id, name, N1, N2, data_real8_2d, MPI_COMM_WORLD)
    
  end subroutine Write_mpi_real

end module write_mpi_data_m
