!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2014 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of fortran-hdf5-interface.
!
! fortran-hdf5-interface is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! fortran-hdf5-interface is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Tests for MPI version of fortran-hdf5-interface: write data
!! @brief
!!
!! @author Fabrice Roy
! ======================================================================

module mpidata_m

  implicit none

  private
  public :: data_integer4_1d, &
       data_integer4_2d, &
       data_integer8_1d, &
       data_integer8_2d, &
       data_real4_1d, &
       data_real4_2d, &
       data_real8_1d, &
       data_real8_2d
  public :: init_mpi_data,&
       N1, N2

  integer, parameter :: ERR_MSG_LEN=256

  integer, parameter :: N1 = 1000
  integer, parameter :: N2 = 1000

  integer(kind=4), allocatable, dimension(:) :: data_integer4_1d
  integer(kind=4), allocatable, dimension(:, :) :: data_integer4_2d
  integer(kind=8), allocatable, dimension(:) :: data_integer8_1d
  integer(kind=8), allocatable, dimension(:, :) :: data_integer8_2d

  real(kind=4), allocatable, dimension(:) :: data_real4_1d
  real(kind=4), allocatable, dimension(:, :) :: data_real4_2d
  real(kind=8), allocatable, dimension(:) :: data_real8_1d
  real(kind=8), allocatable, dimension(:, :) :: data_real8_2d

contains

  subroutine init_mpi_data

    use iso_fortran_env

    integer :: allocstat
    character(len=ERR_MSG_LEN) :: error_message
    logical, save :: initialized=.false.
    integer :: j

    if(.not.initialized) then

       allocate(data_integer4_1d(N1), stat=allocstat, errmsg=error_message)
       if(allocstat/=0) then
          write(ERROR_UNIT,*) 'Allocate failed for data_integer4_1d in init_mpi_data'
          write(ERROR_UNIT,*) error_message
       end if
       allocate(data_integer4_2d(N1,N2), stat=allocstat, errmsg=error_message)
       if(allocstat/=0) then
          write(ERROR_UNIT,*) 'Allocate failed for data_integer4_2d in init_mpi_data'
          write(ERROR_UNIT,*) error_message
       end if
       allocate(data_integer8_1d(N1), stat=allocstat, errmsg=error_message)
       if(allocstat/=0) then
          write(ERROR_UNIT,*) 'Allocate failed for data_integer8_1d in init_mpi_data'
          write(ERROR_UNIT,*) error_message
       end if
       allocate(data_integer8_2d(N1,N2), stat=allocstat, errmsg=error_message)
       if(allocstat/=0) then
          write(ERROR_UNIT,*) 'Allocate failed for data_integer8_2d in init_mpi_data'
          write(ERROR_UNIT,*) error_message
       end if

       allocate(data_real4_1d(N1), stat=allocstat, errmsg=error_message)
       if(allocstat/=0) then
          write(ERROR_UNIT,*) 'Allocate failed for data_real4_1d in init_mpi_data'
          write(ERROR_UNIT,*) error_message
       end if
       allocate(data_real4_2d(N1,N2), stat=allocstat, errmsg=error_message)
       if(allocstat/=0) then
          write(ERROR_UNIT,*) 'Allocate failed for data_real4_2d in init_mpi_data'
          write(ERROR_UNIT,*) error_message
       end if
       allocate(data_real8_1d(N1), stat=allocstat, errmsg=error_message)
       if(allocstat/=0) then
          write(ERROR_UNIT,*) 'Allocate failed for data_real8_1d in init_mpi_data'
          write(ERROR_UNIT,*) error_message
       end if
       allocate(data_real8_2d(N1,N2), stat=allocstat, errmsg=error_message)
       if(allocstat/=0) then
          write(ERROR_UNIT,*) 'Allocate failed for data_real8_2d in init_mpi_data'
          write(ERROR_UNIT,*) error_message
       end if

       do j = 1, N2
          data_integer4_2d(:,j) = j
          data_integer8_2d(:,j) = int(j,kind=8)
          data_real4_2d(:,j) = real(j,kind=4)
          data_real8_2d(:,j) = real(j,kind=8)
       end do
       do j = 1, N1
          data_integer4_1d(j) = j
          data_integer8_1d(j) = int(j,kind=8)
          data_real4_1d(j) = real(j,kind=4)
          data_real8_1d(j) = real(j,kind=8)
       end do
       initialized = .true.
    end if

  end subroutine init_mpi_data
end module mpidata_m
