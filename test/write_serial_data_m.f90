!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2014 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of fortran-hdf5-interface.
!
! fortran-hdf5-interface is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! fortran-hdf5-interface is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Tests for serial version of fortran-hdf5-interface: write data
!! @brief
!!
!! @author Fabrice Roy
! ======================================================================

module write_serial_data_m

  implicit none

  private

  public :: Write_serial_int, &
       Write_serial_real
  
contains

  subroutine Write_serial_int(id)

    use data_m
    use fortran_hdf5_constants_m
    use fortran_hdf5_write_data_m
    use hdf5
    
    integer(hid_t), intent(in) :: id

    character(H5_STR_LEN) :: name

    call init_data
    
    name = 'integer4_1d'
    call Hdf5_write_data(id, name, N1, data_integer4_1d)

    name = 'integer4_2d'
    call Hdf5_write_data(id, name, N1, N2, data_integer4_2d)

    name = 'integer8_1d'
    call Hdf5_write_data(id, name, N1, data_integer8_1d)

    name = 'integer8_2d'
    call Hdf5_write_data(id, name, N1, N2, data_integer8_2d)

    
  end subroutine Write_serial_int

!------------------------------------------------------------------------------------------------------------------------------------

  subroutine Write_serial_real(id)

    use data_m
    use fortran_hdf5_constants_m
    use fortran_hdf5_write_data_m
    use hdf5
    
    integer(hid_t), intent(in) :: id

    character(H5_STR_LEN) :: name

    call init_data
    
    name = 'real4_1d'
    call Hdf5_write_data(id, name, N1, data_real4_1d)

    name = 'real4_2d'
    call Hdf5_write_data(id, name, N1, N2, data_real4_2d)

    name = 'real8_1d'
    call Hdf5_write_data(id, name, N1, data_real8_1d)

    name = 'real8_2d'
    call Hdf5_write_data(id, name, N1, N2, data_real8_2d)
    
  end subroutine Write_serial_real

end module write_serial_data_m
