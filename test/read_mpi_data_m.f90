!------------------------------------------------------------------------------------------------------------------------------------
! Copyright 2014 Fabrice Roy
!
! Contact: fabrice.roy@obspm.fr
!
! This file is part of fortran-hdf5-interface.
!
! fortran-hdf5-interface is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! fortran-hdf5-interface is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with fortran-hdf5-interface.  If not, see <http://www.gnu.org/licenses/>.

!> @file
!! Tests for MPI version of fortran-hdf5-interface: read data
!! @brief
!!
!! @author Fabrice Roy
! ======================================================================

module read_mpi_data_m

  implicit none

  private

  public :: Read_mpi_int, &
       Read_mpi_real
  
contains

  subroutine Read_mpi_int(id)

    use hdf5
    
    integer(hid_t), intent(in) :: id

  end subroutine Read_mpi_int

!------------------------------------------------------------------------------------------------------------------------------------

  subroutine Read_mpi_real(id)

    use hdf5
    
    integer(hid_t), intent(in) :: id

  end subroutine Read_mpi_real

end module read_mpi_data_m
